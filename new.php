<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Document</title>
  <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/css/bootstrap.min.css" rel="stylesheet">
  <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/js/bootstrap.bundle.min.js"></script>
  <link rel="stylesheet" href="new.css">
</head>
<body>
<?php
$pdo = new PDO('mysql:host=localhost;port=3306;dbname=todolist', 'root', '');
if (isset($_POST['title'])) {
    $title = $_POST['title'];
    $nom = $pdo->prepare('INSERT INTO todo (title) VALUES (?)');
    $nom->execute([$title]);
}
if (isset($_POST['r'])) {
    $do = $_POST['r'];
    $stmt = $pdo->prepare("UPDATE todo SET done = 1 - done WHERE id = ?");
    $stmt->execute([$do]);
}
if (isset($_POST['supp'])) {
  $supp = $_POST['supp'];
  $sup = $pdo->prepare("DELETE FROM todo WHERE id = ?");
  $sup->execute([$supp]);
}
$taches = $pdo->query("SELECT * FROM todo ORDER BY created_at DESC")->fetchAll(PDO::FETCH_ASSOC);

?>
<nav class="navbar navbar-expand-lg navbar-dark bg-dark">
  <div class="container-fluid">
    <a class="navbar-brand" href="#">Todo Liste</a>
  </div>
</nav>

  <form method="post" action="#" >
  <div id="div2">
    <input type="text"  name="title" required placeholder="Task Title" id="inp1">
    <button type="submit" class="btn btn-primary" id="inp2">Add</button>
    </div> 
  </form>
  
<table class="table center-table" id="tab1">
<?php foreach ($taches as $t){ ?>
  <tr class="<?php echo ($t['done'] == 0) ? 'table-warning' : 'table-success'; ?>">
    <td id="td1">
    <?php echo $t['title']; ?>
    </td>
    <td>
    <form method="post">
      <div class="btn-group" role="group" aria-label="Basic mixed styles example" id="div1">
      
        <button type="submit" name="r" class="btn btn-primary" value="<?php echo $t['id'] ?>">
        <?php echo ($t['done'] == 0) ? 'undo' : 'done'; ?>
      </button>
        <button type="submit" name="supp" class="btn btn-danger" id="btn1" value="<?php echo $t['id'] ?>">
          <span>×</span>
        </button>
        
      </div>
      </form>
    </td>
  </tr>
  <?php } ?>
</table>
</body>
</html>
